﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    public int number;
    public GameObject effect;

    public int Collect()
    {
        Instantiate(effect, transform.position, Quaternion.identity);
        effect.transform.eulerAngles = transform.eulerAngles;
        gameObject.SetActive(false);
        return number;
    }
}
