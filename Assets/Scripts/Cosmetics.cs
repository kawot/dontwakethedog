﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cosmetics : MonoBehaviour
{
    [SerializeField] Material suitMaterial;
    [SerializeField] Material hairMaterial;
    [SerializeField] Material beardMaterial;
    [SerializeField] Material[] skinMaterials;
    [Space()]
    [SerializeField] Texture[] suitMasks;
    [SerializeField] Texture[] suitPatterns;
    [SerializeField] Texture[] hairs;
    [SerializeField] Texture[] beards;
    [SerializeField] Color[] skinColors;
    [SerializeField] Color[] hairColors;

    void Start()
    {
        Reskin();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) Reskin();
    }

    private void Reskin()
    {
        suitMaterial.SetTexture("_Mask", suitMasks[Random.Range(0, suitMasks.Length)]);
        suitMaterial.SetTexture("_Pattern", suitPatterns[Random.Range(0, suitPatterns.Length)]);
        var hairColor = hairColors[Random.Range(0, hairColors.Length)];
        hairMaterial.SetTexture("_Mask", hairs[Random.Range(0, hairs.Length)]);
        hairMaterial.SetColor("_Tint", hairColor);
        beardMaterial.SetTexture("_Mask", beards[Random.Range(0, beards.Length)]);
        beardMaterial.SetColor("_Tint", hairColor);
        var skinColor = skinColors[Random.Range(0, skinColors.Length)];
        foreach (var mat in skinMaterials) mat.SetColor("_Tint", skinColor);
    }
}
