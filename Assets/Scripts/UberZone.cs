﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UberZone : MonoBehaviour
{
    [SerializeField] Zone zone;

    private void OnTriggerExit2D(Collider2D collision)
    {
        var drop = collision.transform.parent;
        if (drop.CompareTag("drop"))
        {
            var body = collision.attachedRigidbody;
            zone.RespawnDrop(body);
        }
    }
}