﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RecordTimer : MonoBehaviour
{
    const string saveRecordKey = "record";
    const string saveStarKey = "star";

    public static RecordTimer instance;
    public static bool haveFinished = false;
    
    [SerializeField] Image resetButton;
    [SerializeField] TextMeshProUGUI recordLabel;
    [SerializeField] TextMeshProUGUI recordText;
    [SerializeField] TextMeshProUGUI newRecordText;
    [SerializeField] TextMeshProUGUI timerLabel;
    [SerializeField] string timeFormat = "{0:D1}:{1:D2}<size=60>.{2:D2}";
    [Space()]
    [SerializeField] string playerTag = "Player";
    [SerializeField] string starTag = "Star";
    [SerializeField] string finishTag = "Finish";
    [SerializeField] float alertTime = 45f;
    [SerializeField] float starTime = 50f;
    [SerializeField] float alertHalfPeriod = 0.3f;
    [SerializeField] Color alertColor; 
    [SerializeField] float resetAllowedTime = 10f;
    [SerializeField] float restartDelay = 5f;

    [SerializeField] float finishSlowMo = 0.2f;
    [SerializeField] float slowMoTime = 0.4f;
    [SerializeField] float firstTimeDelay = 5f;
    [SerializeField] float movieDelay = 2f;

    [SerializeField] Image[] starImages;
    [SerializeField] Star[] stars;
    [SerializeField] Color noStarColor;

    public TextMeshProUGUI[] tips;

    public Rigidbody2D[] coaches;
    public float coacheDropChance = 0.3f;
    public Gate gate;
    public GameObject zone;

    private float recordTime;
    private float timer;

    private Sequence alertSequence;

    private void Start()
    {
        instance = this;
        haveFinished = false;
        ResetTimers();

        newRecordText.alpha = 0f;
        resetButton.gameObject.SetActive(false);

        Time.timeScale = 1f;
    }

    private void ResetTimers()
    {
        for (int i = 0; i < stars.Length; i++)
        {
            if (!PlayerPrefs.HasKey(saveStarKey + i)) PlayerPrefs.SetInt(saveStarKey + i, 0);
            if (PlayerPrefs.GetInt(saveStarKey + i) != 0) stars[i].gameObject.SetActive(false);
        }
        ShowStars();

        if (!PlayerPrefs.HasKey(saveRecordKey)) PlayerPrefs.SetFloat(saveRecordKey, 60f * 9f + 59.99f);
        recordTime = PlayerPrefs.GetFloat(saveRecordKey);
        TimeSpan timeSpan = TimeSpan.FromSeconds(recordTime);
        string timeText = string.Format(timeFormat, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds / 10);
        recordLabel.text = timeText;
        timer = -movieDelay;
        if (!PlayerPrefs.HasKey("firstPlay")) PlayerPrefs.SetInt("firstPlay", 1);
        if (PlayerPrefs.GetInt("firstPlay") == 1) timer -= firstTimeDelay;
    }

    private void ShowStars()
    {
        for (int i = 0; i < stars.Length; i++)
        {
            if (PlayerPrefs.GetInt(saveStarKey + i) == 0) starImages[i].color = noStarColor;
            else starImages[i].color = Color.white;
        }
    }

    private void Update()
    {
        if (haveFinished) return;

        timer += Time.deltaTime;

        if (timer < 0f) return;

        if (zone.activeSelf == false)
        {
            gate.enabled = true;
            zone.SetActive(true);
            if (PlayerPrefs.GetInt("firstPlay") != 1) DropCoaches();
        }

        TimeSpan timeSpan = TimeSpan.FromSeconds(timer);
        string timeText = string.Format(timeFormat, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds / 10);
        timerLabel.text = timeText;

        if (timer>resetAllowedTime && !resetButton.gameObject.activeSelf)
        {
            resetButton.gameObject.SetActive(true);
            resetButton.color = new Color(1f, 1f, 1f, 0f);
            resetButton.DOFade(1f, 1f).SetEase(Ease.InOutSine);
            foreach(var t in tips)
            {
                t.DOFade(0f, 1f).SetEase(Ease.InOutSine);
            }
        }

        if (timer > alertTime && alertSequence == null && stars[2].gameObject.activeSelf)
        {
            alertSequence = DOTween.Sequence();
            alertSequence
                .Append(timerLabel.DOColor(alertColor, alertHalfPeriod).SetEase(Ease.InOutSine))
                .Append(timerLabel.DOColor(Color.white, alertHalfPeriod).SetEase(Ease.InOutSine))
                .SetLoops(-1);
        }

        if (timer>starTime && stars[2].gameObject.activeSelf)
        {
            alertSequence.Kill();
            timerLabel.DOColor(alertColor, alertHalfPeriod).SetEase(Ease.InOutSine);
            if (stars[2].gameObject.activeSelf) starImages[2].color = alertColor;
            stars[2].gameObject.SetActive(false);
        }
    }

    public void Goal(GameObject obj, GameObject who)
    {
        if (!who.CompareTag(playerTag)) return;
        if (haveFinished) return;

        if (obj.CompareTag(finishTag)) Finish();
        else if (obj.CompareTag(starTag)) CollectStar(obj);
    }

    private void DropCoaches()
    {
        foreach (var body in coaches)
        {
            if (Random.Range(0f, 1f) > 1 - coacheDropChance) body.isKinematic = false;
        }
    }

    private void Finish()
    {
        PlayerPrefs.SetInt("firstPlay", 0);
        alertSequence.Kill();
        timerLabel.DOColor(Color.white, alertHalfPeriod).SetEase(Ease.InOutSine);

        haveFinished = true;
        if (timer < recordTime)
        {
            recordTime = timer;
            PlayerPrefs.SetFloat(saveRecordKey, timer);

            recordText.text = "old record";

            alertSequence = DOTween.Sequence();
            alertSequence
                .Append(newRecordText.DOFade(1f, alertHalfPeriod).SetEase(Ease.InOutSine))
                .Append(newRecordText.DOFade(0f, alertHalfPeriod).SetEase(Ease.InOutSine))
                .SetLoops(-1);
        }
        Time.timeScale = finishSlowMo;
        StartCoroutine(Restart());
    }

    private void CollectStar(GameObject o)
    {
        var star = o.GetComponent<Star>();
        var starIndex = star.Collect();
        PlayerPrefs.SetInt(saveStarKey + starIndex, 1);
        starImages[starIndex].color = Color.white;
    }

    private void OnDestroy()
    {
        alertSequence.Kill();
    }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(slowMoTime);
        Time.timeScale = 1f;
        yield return new WaitForSeconds(restartDelay - movieDelay);
        LevelReset.instance.ResetLevel();
    }

}
