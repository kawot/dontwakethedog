﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "ZoneConfig", menuName = "AquaSlide/ZoneConfig", order = 1)]
public class ZoneSettings : ScriptableObject
{
    public GameObject dropPrefab;
    public int dropCount = 200;
    public int heavyDropRate = 10;
    public float dropRate = 0.01f;
    [Range(0f, 1f)] public float distribution = 0.5f;

}