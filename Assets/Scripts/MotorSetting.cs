﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MotorConfig", menuName = "AquaSlide/MotorConfig", order = 1)]
public class MotorSetting : ScriptableObject
{
    public float motorSpeed = 200f;
    public float motorForce = 10000f;
    public float softAngle = 5f;
}
