﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DropConfig", menuName = "AquaSlide/DropConfig", order = 1)]
public class DropSettings : ScriptableObject
{
    public float startSize = 0.4f;
    public float minScale = 0.1f;
    public Vector2 lifeTime = new Vector2(3f, 10f);

}
