﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuoyancyConfig", menuName = "AquaSlide/BuoyancyConfig", order = 1)]
public class BuoyancySettings : ScriptableObject
{
    public float dropBuoyancyForce = 0.6f;
    public float dropDrag = 0.01f;
    public float directDropDrag = 0.2f;
}
