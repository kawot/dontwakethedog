﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartCamera : MonoBehaviour
{
    [SerializeField] Rigidbody2D target;
    [SerializeField] CameraSettings config;
    [SerializeField] Camera[] cameras;
    [SerializeField] Transform zone;
    [SerializeField] Follow backGround;

    private Vector3 currentVelocity;
    private float currentCamScale;
    private float CamScaleChange;
    private Vector2 currentOffset;
    private Vector2 offsetChange;

    private void Start()
    {
        currentCamScale = 1f;
    }

    void FixedUpdate()
    {
        currentCamScale = Mathf.SmoothDamp(currentCamScale, 1f + target.velocity.magnitude * config.sizeVelocityFactor, ref CamScaleChange, config.cameraResizeSmooth);
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].orthographicSize = config.cameraSize * currentCamScale;
        }
        backGround.Scale(currentCamScale);
        zone.localScale = Vector3.one * currentCamScale;
        currentOffset = Vector2.SmoothDamp(currentOffset, target.velocity * config.offsetVelocityFactor, ref offsetChange, config.offsetSmooth);
        var arg = Vector3.Distance(target.position + currentOffset, transform.position) / config.maxDistance;
        var smooth = config.smoothFromDistance.Evaluate(arg);
        transform.position = Vector3.SmoothDamp(transform.position, target.position + currentOffset, ref currentVelocity, smooth);

        //var speed = target.velocity.magnitude;
        //Debug.Log(speed);
        //if (speed > config.velocityForScream.y) AudioController.instance.Scream((speed - config.velocityForScream.y) / config.velocityForScream.y);
        //else if (speed < config.velocityForScream.x) AudioController.instance.ShutUp();
    }
}
