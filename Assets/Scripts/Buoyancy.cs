﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buoyancy : MonoBehaviour
{
    [SerializeField] private BuoyancySettings config;

    private Rigidbody2D body;
    private List<Rigidbody2D> drops = new List<Rigidbody2D>();

    private void Start()
    {
        body = GetComponent<Rigidbody2D>();
        if (body == null) Debug.LogError("no rigidbody on object");
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < drops.Count; i++)
        {
            var pos = drops[i].transform.position;
            var size = drops[i].transform.localScale.x;
            body.AddForceAtPosition(Vector2.up * config.dropBuoyancyForce * size, pos);
            var relativeVelocity = drops[i].velocity - body.velocity;
            var dragForce = body.mass > 0.1f ? config.directDropDrag : config.dropDrag;
            var drag = dragForce * relativeVelocity.sqrMagnitude * relativeVelocity.normalized;
            body.AddForceAtPosition(drag * size, pos);
            drops[i].AddForce(-drag);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var drop = collision.GetComponentInParent<Rigidbody2D>();
        if (drop == null) { RecordTimer.instance.Goal(collision.gameObject, gameObject); return; }
        drops.Add(drop);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var drop = collision.GetComponentInParent<Rigidbody2D>();
        if (drop == null) { return; }
        drops.Remove(drop);
    }
}
