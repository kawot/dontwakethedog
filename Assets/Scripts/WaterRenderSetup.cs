﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class WaterRenderSetup : MonoBehaviour
{
    [SerializeField] Camera waterCam;
    [Range(0, 4)] [SerializeField] int downsampling = 0;
    [SerializeField] Material waterMaterial;
    private Vector2Int screen;

    private RenderTexture rt;

    void Start()
    {
        SetupWater();
    }

    private void SetupWater()
    {
        screen = new Vector2Int(Screen.width, Screen.height);
        var ds = Mathf.Pow(2, downsampling);
        var desc = new RenderTextureDescriptor(Mathf.RoundToInt(Screen.width / ds), Mathf.RoundToInt(Screen.height / ds), GraphicsFormat.R8G8B8A8_UNorm, 0);
        rt = new RenderTexture(desc);
        waterCam.targetTexture = rt;
        waterMaterial.SetTexture("_dropsTex", rt);
    }

    private void Update()
    {
        if (screen != new Vector2Int(Screen.width, Screen.height))
        {
            rt.Release();
            SetupWater();
        }
    }

    private void OnDestroy()
    {
        rt.Release();
    }

}
