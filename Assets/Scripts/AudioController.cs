﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController instance;

    [SerializeField] AudioSource theme;
    [SerializeField] AudioSource scream;
    [SerializeField] float showTime = 2f;

    float screamVolume;
    float themeVolume;

    void Start()
    {
        instance = this;
        screamVolume = scream.volume;
        themeVolume = theme.volume;
        theme.volume = 0f;
        theme.time = Random.Range(0f, theme.clip.length);
        StartCoroutine(StartThemeCoroutine());
    }

    public void SetThemeVolume(float volume)
    {
        theme.volume = themeVolume * volume;
    }

    public void Scream(float volume)
    {
        scream.volume = screamVolume * volume;

        if (scream.isPlaying) return;

        scream.pitch = Random.Range(0.8f, 1f);
        scream.Play();
    }

    public void ShutUp()
    {
        if (scream.isPlaying) scream.Stop();
    }

    public void MuteTheme()
    {
        StartCoroutine(MuteCoroutine());
    }

    IEnumerator StartThemeCoroutine()
    {
        var timer = 0f;
        while (timer < showTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
            SetThemeVolume(timer / showTime);
        }
    }

    IEnumerator MuteCoroutine()
    {
        var timer = 0f;
        while (timer < showTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
            SetThemeVolume(1f - timer / showTime);
        }
    }
}
