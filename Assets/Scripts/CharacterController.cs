﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterController : MonoBehaviour
{
    //public TextMeshProUGUI testLabel;
    public bool auto = false;
    public SwimmerPose autoPose = SwimmerPose.Star;
    public float autoPeriod = 1f;

    private SwimmerPose currentPose = SwimmerPose.Relax;

    public enum SwimmerPose
    {
        Trout,
        Star,
        Relax
    }

    [SerializeField] Limb[] limbs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (auto) return;

        if (Input.GetMouseButtonDown(0)) currentPose = Input.mousePosition.x < 0.5f * Screen.width ? SwimmerPose.Star : SwimmerPose.Trout;
        if (Input.GetMouseButton(0)) currentPose = Input.mousePosition.x < 0.5f * Screen.width ? SwimmerPose.Star : SwimmerPose.Trout;
        if (Input.GetMouseButtonUp(0)) currentPose = SwimmerPose.Relax;
    }

    private void FixedUpdate()
    {
        if (auto)
        {
            var arg = Mathf.Sin(Time.time * autoPeriod) > 0f;
            if (arg) currentPose = autoPose;
            else currentPose = SwimmerPose.Relax;
        }

        //testLabel.text = currentPose.ToString();
        for (int i = 0; i < limbs.Length; i++)
        {
            limbs[i].ChangePose(currentPose);
        }
    }

    //public void LeftScreenHold()
    //{
    //    Debug.Log("HOLD");
    //}
}
