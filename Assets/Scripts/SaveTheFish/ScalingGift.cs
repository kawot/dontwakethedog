﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalingGift : Gift
{
    [SerializeField] float scaleDuration = 0.5f;
    [SerializeField] Vector3 scalingFactor = Vector3.one;
    [SerializeField] AnimationCurve scaleCurve;
    [SerializeField] GameObject effect;

    private bool isActivated = false;

    public override void Activate()
    {
        if (isActivated) return;

        isActivated = true;
        StartCoroutine(Scale());
    }

    IEnumerator Scale()
    {
        var baseScale = transform.localScale;
        var finalScale = Vector3.Scale(baseScale, scalingFactor);
        var timer = 0f;
        while (timer < 1f)
        {
            timer += Time.deltaTime / scaleDuration;
            var t = scaleCurve.Evaluate(timer);
            transform.localScale = Vector3.Lerp(baseScale, finalScale, t);

            yield return new WaitForEndOfFrame();
        }
        Instantiate(effect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
