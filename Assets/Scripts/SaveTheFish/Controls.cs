﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controls : MonoBehaviour
{
    public abstract Gift GetGift();
}
