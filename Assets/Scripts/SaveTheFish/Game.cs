﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] Controls touchControls;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) TouchGift();
    }

    private void TouchGift()
    {
        var gift = touchControls.GetGift();
        if (gift == null) return;
        gift.Activate();
    }
}

