﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls2D : Controls
{
    [SerializeField] LayerMask giftMask;

    private Camera cam;

    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override Gift GetGift()
    {
        var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0f;
        var hit = Physics2D.Raycast(mousePos, Vector2.zero, 0f, giftMask);
        if (!hit)
        {
            return null;
        }
        var gift = hit.collider.GetComponent<Gift>();
        if (gift == null) Debug.LogError("no Gift component on tapped object: " + hit.collider.gameObject.name);
        return gift;
    }
}
