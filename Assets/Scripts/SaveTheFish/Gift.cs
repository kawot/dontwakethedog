﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gift : MonoBehaviour
{
    public abstract void Activate();
}


