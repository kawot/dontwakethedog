﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour
{
    [SerializeField] Rigidbody2D body;
    [SerializeField] float disturbance = 1f;
    [SerializeField] DropSettings config;

    private float lifeTime;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        Resize();
    }

    public void Resize()
    {
        lifeTime = Random.Range(config.lifeTime.x, config.lifeTime.y);
        timer = lifeTime;
    }

    void FixedUpdate()
    {
        if (disturbance != 0) body.AddForce(Random.insideUnitCircle * disturbance);

        timer -= Time.fixedDeltaTime;
        transform.localScale = Vector3.one * config.startSize * (timer / lifeTime);
        if (timer / lifeTime < config.minScale)
        {
            Resize();
            transform.position += Vector3.up * 100f;
        }
    }

}
