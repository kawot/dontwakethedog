﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls : Controls
{
    [SerializeField] float maxDistance;
    [SerializeField] LayerMask giftMask;

    private Camera cam;

    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override Gift GetGift()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, maxDistance, giftMask))
        {
            var gift = hit.collider.GetComponent<Gift>();
            if (gift == null) Debug.LogError("no Gift component on tapped object: " + hit.collider.gameObject.name);
            return gift;
        }
        else return null;
    } 
}
