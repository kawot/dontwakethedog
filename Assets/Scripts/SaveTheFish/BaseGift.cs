﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGift : Gift
{
    [SerializeField] GameObject effect;

    public override void Activate()
    {
        Instantiate(effect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
