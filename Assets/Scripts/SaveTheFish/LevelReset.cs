﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelReset : MonoBehaviour
{
    public static LevelReset instance;

    [SerializeField] float resetDelay = 2f;
    [SerializeField] Movie movie;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetLevel(bool withMovie = true)
    {
        var delay = withMovie ? movie.showTime : 0f;
        StartCoroutine(DelayedReset(delay));
    }

    IEnumerator DelayedReset(float delay)
    {
        if (delay != 0f)
        {
            movie.Hide();
            AudioController.instance.MuteTheme();
        }
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(0);
    }
}
