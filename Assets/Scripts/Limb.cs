﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Limb : MonoBehaviour
{
    [SerializeField] float troutAngle;
    [SerializeField] float starAngle;
    [SerializeField] MotorSetting config;
    [Space()]
    [SerializeField] HingeJoint2D joint;
    [SerializeField] Transform target;
    [Space()]
    [SerializeField] bool debug = true;

    //public TextMeshProUGUI testLabelPose;
    //public TextMeshProUGUI testLabel;


    public void ChangePose(CharacterController.SwimmerPose pose)
    {
        //if (testLabelPose != null) testLabelPose.text = pose.ToString();

        switch (pose)
        {
            case CharacterController.SwimmerPose.Trout:
                Force(troutAngle);
                break;
            case CharacterController.SwimmerPose.Star:
                Force(starAngle);
                break;
            case CharacterController.SwimmerPose.Relax:
                Relax();
                break;
            default:
                Debug.LogError("unknown pose");
                break;
        }
    }
    
    private void Relax()
    {
        joint.useMotor = false;

        //if (testLabel!=null) testLabel.text = "RELAXING";
    }

    private void Force (float a)
    {
        joint.useMotor = true;

        var curA = Vector2.SignedAngle(target.up, transform.up);
        var dif = curA - a;
        if (dif > 180f) dif -= 360f;
        else if (dif < -180f) dif += 360f;
        //if (debug) Debug.Log(dif);
        var soft = Mathf.Abs(dif) > config.softAngle ? 1f : Mathf.Abs(dif) / config.softAngle;
        var dir = dif < 0f ? -1f : 1f;
        var m = joint.motor;
        m.motorSpeed = config.motorSpeed * dir * soft * soft;
        m.maxMotorTorque = config.motorForce * soft * soft;
        joint.motor = m;
    }
}
