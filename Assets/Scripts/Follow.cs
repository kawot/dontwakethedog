﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform target;
    private Vector3 startScale;

    private void Start()
    {
        startScale = transform.localScale;
    }

    void FixedUpdate()
    {
        var pos = transform.position;
        pos.x = target.position.x;
        pos.y = target.position.y;
        transform.position = pos;
    }

    public void Scale(float scale)
    {
        transform.localScale = startScale * scale;
    }
}
