﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraConfig", menuName = "AquaSlide/CameraConfig", order = 1)]
public class CameraSettings : ScriptableObject
{
    public float smoothness = 1f;
    public AnimationCurve smoothFromDistance;
    public float maxDistance;
    public float offsetVelocityFactor = 1f;
    public float offsetSmooth = 0.3f;
    public float sizeVelocityFactor = 1f;
    public float cameraSize = 8f;
    public float cameraResizeSmooth = 1f;
    public Vector2 velocityForScream = new Vector2(1f, 5f);
}
