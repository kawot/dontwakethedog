﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movie : MonoBehaviour
{
    [SerializeField] Material mat;
    [SerializeField] Image image;
    [SerializeField] AnimationCurve showCurve;
    public float showTime;

    private void Start()
    {
        SetMovie(0f);
        Show();
    }

    public void SetMovie(float arg)
    {
        mat.SetFloat("_Amount", showCurve.Evaluate(arg));
    }

    public void Show()
    {
        StartCoroutine(ShowCoroutine());
    }

    public void Hide()
    {
        StartCoroutine(HideCoroutine());
    }

    IEnumerator ShowCoroutine()
    {
        var timer = 0f;
        while (timer < showTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
            SetMovie(timer / showTime);
        }
        image.enabled = false;
    }

    IEnumerator HideCoroutine()
    {
        var timer = 0f;
        image.enabled = true;
        while (timer < showTime)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
            SetMovie(1f - timer / showTime);
        }
    }


}
