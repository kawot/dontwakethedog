﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    [SerializeField] ZoneSettings config;

    private List<Fountain> streams = new List<Fountain>();
    private List<Fountain> pushers = new List<Fountain>();

    void Start()
    {
        StartCoroutine(GenerateWater());
    }


    IEnumerator GenerateWater()
    {

        for (int i = 0; i < config.dropCount; i++)
        {

            yield return new WaitForSeconds(config.dropRate);
            var body = Instantiate(config.dropPrefab).GetComponent<Rigidbody2D>();
            if (i % config.heavyDropRate == 0) body.mass *= 1.01f;
            body.gameObject.SetActive(true);
            RespawnDrop(body);

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var source = collision.transform.parent;
        if (source.CompareTag("drop")) return;

        //Debug.Log("new source " + collision.gameObject.name);
        var fountain = collision.GetComponent<Fountain>();
        if (fountain.stream) streams.Add(fountain);
        else pushers.Add(fountain);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var drop = collision.transform.parent;
        if (drop.CompareTag("drop"))
        {
            var body = collision.attachedRigidbody;
            RespawnDrop(body);
        }
        else
        {
            var fountain = collision.GetComponent<Fountain>();
            if (fountain.oneTime) fountain.gameObject.SetActive(false);
            if (fountain.stream) streams.Remove(fountain);
            else pushers.Remove(fountain);
        }
    }

    public void RespawnDrop(Rigidbody2D body)
    {
        if (RecordTimer.haveFinished)
        {
            body.gameObject.SetActive(false);

            return;
        }

        var sourceIsStream = (Random.Range(0f, 1f) < config.distribution || pushers.Count == 0) ? true : false;
        Fountain source;
        if (sourceIsStream)
        {
            if (streams.Count == 0) { WildThrow(body); return; }
            //source = streams[0];
            source = streams[Random.Range(0, streams.Count)];
        }
        else
        {
            source = pushers[Random.Range(0, pushers.Count)];
        }

        body.transform.position = source.transform.position;
        body.velocity = source.transform.up * source.startVelocity;
    }

    private void WildThrow(Rigidbody2D body)
    {
        body.transform.position = transform.position + Vector3.up * 10f * transform.localScale.y;
        body.velocity = Vector2.down * 10f;
    }
}
