﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] HingeJoint2D[] doors;
    [SerializeField] float delay = 3f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DelayedOpen());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DelayedOpen()
    {
        yield return new WaitForSeconds(delay);
        OpenGate();
    }

    public void OpenGate()
    {
        foreach (var door in doors)
        {
            door.useMotor = false;
            //door.useLimits = false;
        }
    }
}
